import { start, onUpdate } from './simulation/main';
import { GenteicAlgorithm } from './genetic-algorithm/index';
import { ISimulationConfig } from './simulation-config';


const simulationConfig: ISimulationConfig = {
  car: {
    total: 10,
    wheels: {
      total: 2,
      density: {
        max: 30,
        min: 1
      },
      radius: {
        min: 30,
        max: 60
      }
    },
    chassis: {
      density: {
        max: 30,
        min: 1
      },
      points: 12
    }
  }
};

const genetic = new GenteicAlgorithm(simulationConfig);
genetic.generateFirstGeneration();

start(genetic.cars);
onUpdate((data: any) => {
  genetic.evolve(data);
  start(genetic.cars);
});