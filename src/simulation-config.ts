export interface IWheelsSimulationConfig {
  total: number;
  radius: {
    max: number;
    min: number;
  },
  density: {
    max: number;
    min: number;
  }
}

export interface IChassisSimulationConfig {
  points: number;
  density: {
    max: number;
    min: number;
  }
}

export interface ICarSimulationConfig {
  total: number;
  wheels: IWheelsSimulationConfig;
  chassis: IChassisSimulationConfig;
}

export interface ISimulationConfig {
  car: ICarSimulationConfig;
}