import { ICarSimulationConfig } from '../simulation-config';
import { randomInt } from 'mathjs';
import { ICarConfig } from '../simulation/car/car';

function randomWheelRadius(car: ICarSimulationConfig): number {
  const { min, max } = car.wheels.radius;
  return randomInt(min, max) / 100;
}

function randomWheelDensity(car: ICarSimulationConfig): number {
  const { min, max } = car.wheels.density;
  return randomInt(min, max);
}

function randomChassisDensity(car: ICarSimulationConfig): number {
  const { min, max } = car.chassis.density;
  return randomInt(min, max);
}

export function randomCar(car: ICarSimulationConfig, id: number): ICarConfig {
  return {
    id,
    maxHealth: 600,
    position: {
      x: 2,
      y: 3
    },
    chassis: {
      points: Array(12).fill(0).map(
        item => Math.random() * 1
      ),
      density: randomChassisDensity(car)
    },
    engine: {
      motorSpeed: 20
    },
    wheels: [
      {
        radius: randomWheelRadius(car),
        vertex: randomInt(0, 7),
        density: randomWheelDensity(car)
      },
      {
        radius: randomWheelRadius(car),
        vertex: randomInt(0, 7),
        density: randomWheelDensity(car)
      }
    ]
  };
}