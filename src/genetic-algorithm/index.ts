import { ISimulationConfig } from '../simulation-config';
import { ICarConfig, ICar } from '../simulation/car/car';
import { randomCar } from './random-car';

export class GenteicAlgorithm {

  private _cars: ICarConfig[];

  constructor(private config: ISimulationConfig) {}

  public get cars() { return this._cars; }

  public generateFirstGeneration(): void {
    this._cars = Array(this.config.car.total).fill(0)
      .map((index: number) => randomCar(this.config.car, index));
  }

  public evolve(cars: ICar[]) {
    this.generateFirstGeneration();
  }

}