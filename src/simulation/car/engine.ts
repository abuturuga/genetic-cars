import { Wheel } from "./wheel";
import { Chassis } from "./chassis";
import { config } from '../config';


export interface IEngineConfig {
  motorSpeed: number;
}

export class Engine {

  private _carMass: number = 0;

  constructor(private config: IEngineConfig) {}

  /**
   * Set the entire car mass.
   * 
   * @param carMass Mass of the wheels and chassis
   */
  public setCarMass(carMass: number) {
    this._carMass = carMass;
  }

  /**
   * Attach the engine to the world.
   * 
   * @param wheels Car wheels
   * @param chassis Car chassis
   * @param world Simulation World
   */
  public attach(wheels: Wheel[], chassis: Chassis, world: Box2D.Dynamics.b2World) {
    wheels.forEach(wheel => {
      const jointDef = new Box2D.Dynamics.Joints.b2RevoluteJointDef();
      const torque = this._carMass * -config.gravity / wheel.radius;
      const vertex = chassis.vertices[wheel.vertexIndex()];

      jointDef.localAnchorA.Set(vertex.x, vertex.y);
      jointDef.localAnchorB.Set(0, 0);
      jointDef.bodyA = chassis.body;
      jointDef.bodyB = wheel.body;

      jointDef.maxMotorTorque = torque;
      jointDef.motorSpeed = -this.config.motorSpeed;
      jointDef.enableMotor = true;

      world.CreateJoint(jointDef);
    });
  }
}