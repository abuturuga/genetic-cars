export interface IWheelConfig {
  radius: number;
  vertex: number;
  density: number;
}

export class Wheel {
  
  private _bodyDef: Box2D.Dynamics.b2BodyDef
  private _body: Box2D.Dynamics.b2Body;
  private _fixDef: Box2D.Dynamics.b2FixtureDef;

  constructor(private config: IWheelConfig) {
    this._bodyDef = new Box2D.Dynamics.b2BodyDef();
    this._fixDef = new Box2D.Dynamics.b2FixtureDef();
    this._bodyDef.type = Box2D.Dynamics.b2Body.b2_dynamicBody;
  }

  public get body(): Box2D.Dynamics.b2Body { return this._body; }
  public get radius(): number { return this.config.radius; }

  public vertexIndex(): number {
    return this.config.vertex;
  }

  public setPosition(x: number, y: number): void {
    this._bodyDef.position.Set(x, y);
  }

  public attachToWorld(world: Box2D.Dynamics.b2World): void {
    this._body = world.CreateBody(this._bodyDef);
  }

  public init(): void {
    this._fixDef = new Box2D.Dynamics.b2FixtureDef();
    this._fixDef.shape = new Box2D.Collision.Shapes.b2CircleShape(this.config.radius);
    this._fixDef.density = this.config.density;
    this._fixDef.friction = 1;
    this._fixDef.restitution = 0.2;
    this._fixDef.filter.groupIndex = -1;

    this._body.CreateFixture(this._fixDef);
  }
} 