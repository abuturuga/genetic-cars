export interface IChassisConfig {
  points: number[];
  density: number;
}

export class Chassis {

  private _bodyDef: Box2D.Dynamics.b2BodyDef
  private _body: Box2D.Dynamics.b2Body;
  private _vertices: Box2D.Common.Math.b2Vec2[];

  constructor(private config: IChassisConfig) {
    this._bodyDef = new Box2D.Dynamics.b2BodyDef();
    this._bodyDef.type = Box2D.Dynamics.b2Body.b2_dynamicBody;
    this._vertices = [];
  }

  public get vertices(): Box2D.Common.Math.b2Vec2[] { return this._vertices; }
  public get body(): Box2D.Dynamics.b2Body { return this._body; }

  public setPosition(x: number = 0, y: number = 0): void {
    this._bodyDef.position.Set(x, y);
  }

  public attachToWorld(world: Box2D.Dynamics.b2World): void {
    this._body = world.CreateBody(this._bodyDef);
  }

  public init(): void {
    this.buildVertices(this.config.points);
    const { length } = this._vertices;

    for (let i = 0; i < length - 1; i++) {
      this.bindPart(this._vertices[i], this._vertices[i + 1]);
    }
    this.bindPart(this._vertices[length - 1], this._vertices[0]);
  }

  private buildVertices(points: number[]): void {
    const { b2Vec2 } = Box2D.Common.Math;

    this._vertices.push(new b2Vec2(points[0], 0));
    this._vertices.push(new b2Vec2(points[1], points[2]));
    this._vertices.push(new b2Vec2(0, points[3]));
    this._vertices.push(new b2Vec2(-points[4], points[5]));
    this._vertices.push(new b2Vec2(-points[6], 0));
    this._vertices.push(new b2Vec2(-points[7], -points[8]));
    this._vertices.push(new b2Vec2(0, -points[9]));
    this._vertices.push(new b2Vec2(points[10], -points[11]));
  }

  /**
   * Create a part fixture
   * @param vertex1 First point
   * @param vertex2 Second point
   */
  private createPart(
    vertex1: Box2D.Common.Math.b2Vec2,
    vertex2: Box2D.Common.Math.b2Vec2
  ): Box2D.Dynamics.b2FixtureDef {
    const vertices: Box2D.Common.Math.b2Vec2[] = [];
    vertices.push(vertex1);
    vertices.push(vertex2);
    vertices.push(Box2D.Common.Math.b2Vec2.Make(0, 0));

    const polygon = Box2D.Collision.Shapes.b2PolygonShape.AsArray(vertices, 3);
    
    const fixture = new Box2D.Dynamics.b2FixtureDef();
    fixture.density = this.config.density;
    fixture.friction = 10;
    fixture.restitution = 0.2;
    fixture.filter.groupIndex = -1;
    fixture.shape = polygon;

    return fixture;
  }

  /**
   * Create a fixture
   * @param vertex1 First point
   * @param vertex2 Second point
   */
  private bindPart(
    vertex1: Box2D.Common.Math.b2Vec2,
    vertex2: Box2D.Common.Math.b2Vec2
  ): void {
    const fixture = this.createPart(vertex1, vertex2);
    this._body.CreateFixture(fixture);
  }
} 