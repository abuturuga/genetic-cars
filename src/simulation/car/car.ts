import { IPoint } from '../common/point';
import { Chassis, IChassisConfig } from './chassis';
import { Wheel, IWheelConfig } from './wheel';
import { Engine , IEngineConfig} from './engine';
import { timingSafeEqual } from 'crypto';

export interface ICarConfig {
  position: IPoint;
  maxHealth: number;
  chassis: IChassisConfig;
  wheels: IWheelConfig[];
  engine: IEngineConfig;
  id?: number;
}

export interface ICarState {
  health?: number;
  maxPosition?: IPoint;
  minPosition?: IPoint;
  isLeader?: boolean;
}

export interface ICar {
  state: ICarState;
  config: ICarConfig;
}

export class Car {

  private _chassis: Chassis;

  private _engine: Engine;

  private _wheels: Wheel[];

  private _carMass: number = 0;

  private _state: ICarState;

  public constructor(private config: ICarConfig) {
    this._chassis = new Chassis(config.chassis);
    this._engine = new Engine(config.engine);

    this.initState();
  }

  public get state(): ICarState { return this._state; }

  public set state(state: ICarState) {
    this._state = Object.assign({}, this._state, state);
  }
  
  public getConfig(): ICarConfig {
    return this.config;
  }
 
  /**
   * Attach the car along with all its components into the world.
   * 
   * @param world Simulation world
   */
  public attachToWorld(world: Box2D.Dynamics.b2World) {
    const { x, y } = this.config.position;
    this._chassis.setPosition(x, y);
    this._chassis.attachToWorld(world);

    this.init();
    this.buildWheels(world);
    this.computeCarMass();

    this._engine.setCarMass(this._carMass);
    this._engine.attach(this._wheels, this._chassis, world);
  }

  /**
   * Get the car position relative to the world
   */
  public getPosition(): Box2D.Common.Math.b2Vec2 {
    return this._chassis.body.GetPosition();
  }

  /**
   * Updated the car state for each call.
   * This function must be used inside the game loop.
   */
  public update(): void {
    const { x, y } = this.getPosition();
    const altitudeChanged:boolean = this.updateAltitude(y);

    if (x > this._state.maxPosition.x + 0.01) {
      this._state.health = this.config.maxHealth;
      this._state.maxPosition.x = x;
    } else if (!altitudeChanged) {
      if (this._chassis.body.GetLinearVelocity().x < 0.001){
        this._state.health -= 5;
      } else {
        this._state.health--;
      }
    }
  }

  public isAlive() {
    return this._state.health > 0;
  }

  /**
   * Remove the car from the simulation.
   * @param world Box2D world
   */
  public destroy(world: Box2D.Dynamics.b2World): void {
    world.DestroyBody(this._chassis.body);

    this._wheels.forEach((wheel: Wheel) => {
      world.DestroyBody(wheel.body);
    });
  }

  private updateAltitude(y: number): boolean {
    if (y > this._state.maxPosition.y) {
      this._state.maxPosition.y = y;
      return true;
    } else if (y < this._state.minPosition.y) {
      this._state.minPosition.y = y;
      return true;
    }

    return false;
  }

  private initState(): void {
    this._state = {
      health: this.config.maxHealth,
      maxPosition: { x: 0, y: 0 },
      minPosition: { x: 0, y: 0}
    }
  }

  private buildWheels(world: Box2D.Dynamics.b2World): void {
    const { x, y } = this.config.position;

    this._wheels = this.config.wheels.map((config: IWheelConfig) => {
      const instance = new Wheel(config);
      instance.setPosition(x, y);
      instance.attachToWorld(world);
      instance.init();
      return instance;
    })
  }

  private init(): void {
    this._chassis.init();
  }

  /**
   * Sum the mass of wheels in chassis in order to compute
   * the total mass of the car
   */
  private computeCarMass(): void {
    this._carMass = this._chassis.body.GetMass();

    this._carMass += this._wheels.reduce((acc: any, item: Wheel) => {
      acc += item.body.GetMass();
      return acc;
    }, 0);
  }

}