import { Camera } from './camera';
import { IPoint } from './common/point';
import { config } from './config';
import { CanvasContainer } from './containers/canvas-container';
import { Car, ICarConfig } from './car/car';
import { Ground } from './ground';
import { CarsManager } from './cars-manager';

const scale = 100;

const { b2Vec2 } = Box2D.Common.Math;
const { b2World } = Box2D.Dynamics;

const gravity = new b2Vec2(0, -9.8);
const world = new b2World(gravity, true);


const canvasContainer = new CanvasContainer(config.canvasConfig);
const camera = Camera.getInstance();

const ctx = canvasContainer.ctx;
const canvas_width = canvasContainer.width;
const canvas_height = canvasContainer.height;

const carsManager = new CarsManager();

let cars: any[];

const ground = new Ground({
  maxTiles: 400, 
  dimension: {x: 100 / config.scale, y: 5 / config.scale },
  windowWidth: config.canvasConfig.width / config.scale
});

ground.build(world);

const { b2Body } = Box2D.Dynamics;
const { b2FixtureDef, b2BodyDef } = Box2D.Dynamics;
const { b2PolygonShape } = Box2D.Collision.Shapes;


const { b2DebugDraw } = Box2D.Dynamics;
var debugDraw = new b2DebugDraw;
debugDraw.SetSprite(ctx);
debugDraw.SetDrawScale(scale);
debugDraw.SetFillAlpha(0.5);
debugDraw.SetLineThickness(1.0);
debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
 
world.SetDebugDraw(debugDraw);

let callbackUpdate: Function = (): void => null;

function step() 
{
    var timeStep = 1.0 / config.fps;
    world.Step(timeStep , 8 , 3);
    world.ClearForces();
    canvasContainer.draw(world);
}

function loop(): void {
  let carsList = carsManager.filterCars(cars, world);
  const car = carsManager.getLider(cars);
  carsManager.updateCarsState(cars);
  if (car) {
    camera.position = car.getPosition();
  }

  if (carsList.length === 0) {
    console.log('here');
    callbackUpdate(carsManager.cars);
    stop();
  }

  step();
}

let interval: number;

export function start(carsConfig: ICarConfig[]) {
  cars = carsManager.buildCars(carsConfig, world);
  interval = window.setInterval(() => {
    loop();
  }, 1000 / 60);
}

export function onUpdate(callback: Function) {
  callbackUpdate = callback;
}

export function stop() {
  window.clearInterval(interval);
}