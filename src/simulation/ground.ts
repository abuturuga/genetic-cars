import { IPoint } from "./common/point";
import { config } from './config';

export interface IGroundConfig {
  maxTiles: number;
  dimension: IPoint;
  windowWidth: number;
}

interface IGTileShape {
  m_vertices?: any[]
}

const { b2Vec2 } = Box2D.Common.Math;
const { b2FixtureDef, b2BodyDef } = Box2D.Dynamics;
const { b2PolygonShape } = Box2D.Collision.Shapes;

export class Ground {

  constructor(private config: IGroundConfig) {}

  public build(world: Box2D.Dynamics.b2World) {
    let tilePosition = new b2Vec2(-5, 2);
    let lastTile;

    for (let i = 0; i < this.config.maxTiles; i++) {
      let angle = ((Math.random() * 3) - 1.5) * 1.5 * i / this.config.maxTiles;
      lastTile = this.createTile(tilePosition, angle, this.config.dimension, world);
      let lastFixture = lastTile.GetFixtureList();
      let shape = lastFixture.GetShape() as IGTileShape;
    
      tilePosition = lastTile.GetWorldPoint(shape.m_vertices[3]);
    }
  }

  public buildFlat(world: Box2D.Dynamics.b2World): void {
    var bodyDef = new b2BodyDef();
     
    var fixDef = new b2FixtureDef();
    fixDef.density = 1.0;
    fixDef.friction = 1.0;
    fixDef.restitution = 0.5;
     
    const shape = new b2PolygonShape; 

    shape.SetAsBox(1000 , 1);
    fixDef.shape = shape; 
    bodyDef.position.Set(10.10 , .1);
    world.CreateBody(bodyDef).CreateFixture(fixDef);
  }

  private createTile(
    position: Box2D.Common.Math.b2Vec2,
    angle: number,
    dimension: IPoint,
    world: Box2D.Dynamics.b2World): Box2D.Dynamics.b2Body {

    const bodyDef = new Box2D.Dynamics.b2BodyDef();
    bodyDef.position.Set(position.x, position.y);
    let vertices = this.buildTileVertices(dimension, angle, { x: 0, y: 0 });

    const polygon = Box2D.Collision.Shapes.b2PolygonShape.AsArray(vertices, vertices.length);

    const fixDef = new Box2D.Dynamics.b2FixtureDef();
    fixDef.shape = polygon;
    fixDef.friction = 0.5;

    const body = world.CreateBody(bodyDef);
    body.CreateFixture(fixDef);

    return body;
  }

  private buildTileVertices(dimension: IPoint, angle: number, center: IPoint) {
    const vertices: Box2D.Common.Math.b2Vec2[] = [];
    const { b2Vec2 } = Box2D.Common.Math;

    vertices.push(new b2Vec2(0, 0));
    vertices.push(new b2Vec2(0, -dimension.y));
    vertices.push(new b2Vec2(dimension.x, -dimension.y));
    vertices.push(new b2Vec2(dimension.x, 0));

    return this.rotateTile(vertices, center, angle);
  }
  
  private rotateTile(
    vertices: Box2D.Common.Math.b2Vec2[],
    center: IPoint,
    angle: number) {
    
    const { b2Vec2 } = Box2D.Common.Math;
    
    return vertices.map((vertex: Box2D.Common.Math.b2Vec2) => {
      return new b2Vec2(
        Math.cos(angle) * (vertex.x - center.x) - Math.sin(angle) * (vertex.y - center.y) + center.x,
        Math.sin(angle) * (vertex.x - center.x) + Math.cos(angle) * (vertex.y - center.y) + center.y,
      );
    });
  }

}