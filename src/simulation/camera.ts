import { IPoint } from './common/point';
import { config } from './config';

/**
 * This singleton holds camera position and zoom level.
 */
export class Camera {

  private static _instance: Camera;

  /**
   * Focus position on the screen
   */
  private _position: IPoint;

  /**
   * Zoom level
   */
  private _zoom: number;

  private constructor() {
    this._zoom = 1;
    this._position = { x: 0, y: 0 };
  }

  public static getInstance(): Camera {
    if (!this._instance) {
      this._instance = new Camera();
      return this._instance;
    }

    return this._instance;
  }

  /**
   * Get the camera position, interpolates the x value.
   * (Multiply the x value with the zoom value)
   */
  get position(): IPoint {
    const { x, y } = this._position;

    return {
      x: x * config.scale * this._zoom,
      y: y * config.scale * this._zoom
    };
  }

  set position(position) { this._position = position; }

  get zoom(): number { return this._zoom; }
  set zoom(zoom: number) { this._zoom = zoom; }

}