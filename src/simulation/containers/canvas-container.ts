import { Camera } from '../camera';

/**
 * Config for the canvas container
 */
export interface ICanvasConfig {

  /**
   * CSS selector for canvas element
   */
  selector: string;

  /**
   * Canvas element width
   */
  width: number;

  /**
   * Canvas element height
   */
  height: number;
}

export class CanvasContainer {

  private _element: HTMLCanvasElement;

  private _ctx: CanvasRenderingContext2D;

  private _width: number;

  private _height: number;

  private _camera: Camera;

  constructor(config: ICanvasConfig) {
    this._element = document.querySelector(config.selector) as HTMLCanvasElement;
    this._width = config.width;
    this._height = config.height;
    this._camera = Camera.getInstance();

    this.init();
  }

  public set width(width: number) { this._width = width; }
  public get width(): number { return this._width; }

  public set height(height: number) { this._height = height; }
  public get height(): number { return this._height; }

  public get ctx(): CanvasRenderingContext2D { return this._ctx; }

  public draw(world: Box2D.Dynamics.b2World): void {
    this._ctx.clearRect(0 , 0 , this._width, this._height);

    this._ctx.fillStyle = '#FFF4C9';
    this._ctx.fillRect(0, 0, this._width, this._height);

    this._ctx.save();
    this.setCamera();
    world.DrawDebugData();
    this._ctx.restore();
  }

  private setCamera() {
    const viewCenter = this._width / 2;
    const x = viewCenter - this._camera.position.x;
    const y = this._height / 2 + this._camera.position.y;
    this._ctx.translate(x, y);

    const zoom = this._camera.zoom;
    this._ctx.scale(zoom , -zoom);
  }

  /**
   * Extract the canvas context and set the element dimensions
   */
  private init(): void {
    this._element.width = this._width;
    this._element.height = this._height;
    this._ctx = this._element.getContext('2d') as CanvasRenderingContext2D;
  }

}
