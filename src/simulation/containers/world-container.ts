import { config } from '../config';

export class WorldContainer {

  private _gravity: Box2D.Common.Math.b2Vec2;

  private _world: Box2D.Dynamics.b2World;

  private _doSleep: boolean = true;

  constructor(private ctx: CanvasRenderingContext2D, private scale: number) {
    this._gravity = new Box2D.Common.Math.b2Vec2(0.0, -9.81);
    this._world = new Box2D.Dynamics.b2World(this._gravity, this._doSleep);
    this.setDebugDraw();
  }

  public get world(): Box2D.Dynamics.b2World { return this._world; }

  public step(): void {
    this._world.Step(1.0 / 60, 8, 3);
    this._world.ClearForces();
  }

  private setDebugDraw(): void {
    const { b2DebugDraw } = Box2D.Dynamics;
    const debugDraw = new b2DebugDraw();
    debugDraw.SetSprite(this.ctx);
    debugDraw.SetDrawScale(this.scale);
    debugDraw.SetFillAlpha(0.5);
    debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);

    this._world.SetDebugDraw(debugDraw);
  }

}
