import { ICanvasConfig } from './containers/canvas-container';

export interface IConfig {
  canvasConfig: ICanvasConfig;
  scale: number;
  fps: number;
  gravity: number;
}

export const config: IConfig = {
  canvasConfig: {
    width: window.innerWidth,
    height: window.innerHeight,
    selector: "#canvas",
  },
  gravity: -9.8,
  scale: 100,
  fps: 60
};
