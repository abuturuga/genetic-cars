import { IPoint } from '../common/point';
import { config } from '../config';

const { scale } = config;

export enum BodyType { Dynamic, Kinematic, Static }

export class Body {

  public density: number = 1.0;
  public friction: number = 0.6;
  public restitution: number = 0;

  public linearDamping: number = 0.0;
  public angularDamping: number = 0.0;

  protected _fixDef: Box2D.Dynamics.b2FixtureDef;

  protected _bodyDef: Box2D.Dynamics.b2BodyDef;

  protected _position: IPoint;

  protected _type: BodyType =  BodyType.Dynamic;

  public body: Box2D.Dynamics.b2Body;

  constructor() {
    this._bodyDef = new  Box2D.Dynamics.b2BodyDef();
    this._fixDef = new Box2D.Dynamics.b2FixtureDef();
    this._position = { x: 0, y: 0 };
  }

  public attachToWorld(world: Box2D.Dynamics.b2World): void {
    this.init();
    this.body = world.CreateBody(this._bodyDef);
    this.body.CreateFixture(this._fixDef);
  }

  public set type(type: BodyType) { this._type = type; }
  public get type(): BodyType { return this._type; }

  public set position(position: IPoint) { this._position = position; }
  public get position(): IPoint { return this._position; }

  protected init(): void {
    this._fixDef.density = this.density;
    this._fixDef.friction = this.friction;
    this._fixDef.restitution = this.restitution;

    const { x, y } = this._position;
    this._bodyDef.position.Set(x / scale, y / scale);
    this._bodyDef.linearDamping = this.linearDamping;
    this._bodyDef.angularDamping = this.angularDamping;

    this.setBodyType();
  }

  protected setBodyType(): void {
    const { b2Body } = Box2D.Dynamics;

    switch (this._type) {
      case BodyType.Static:
        this._bodyDef.type = b2Body.b2_staticBody;
        break;
      case BodyType.Kinematic:
        this._bodyDef.type = b2Body.b2_kinematicBody;
        break;
      case BodyType.Dynamic:
        this._bodyDef.type = b2Body.b2_dynamicBody;
        break;
    }
  }

}
