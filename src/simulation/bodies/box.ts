import { config } from '../config';
import { Body } from './body';

const { b2PolygonShape } = Box2D.Collision.Shapes;
const { scale } = config;

export class Box extends Body {

  private _width: number;
  private _height: number;

  constructor() {
    super();
  }

  public set width(width: number) { this._width = width / scale; }
  public get width(): number { return this._width; }

  public set height(height: number) { this._height = height / scale; }
  public get height(): number { return this._height; }

  public setSize(width: number, height: number) {
    this.width = width;
    this.height = height;
  }

  protected init(): void {
    super.init();
    this._fixDef.shape = b2PolygonShape.AsBox(this._width, this._height);
  }

}
