import { IPoint } from '../common/point';
import { config } from '../config';
import { Body } from './body';

const { b2PolygonShape } = Box2D.Collision.Shapes;
const { b2Vec2 } = Box2D.Common.Math;
const { scale } = config;

export class Polygon extends Body {

  private _vertices: Box2D.Common.Math.b2Vec2[];

  constructor() {
    super();
  }

  public setPoints(points: IPoint[]): void {
    this._vertices = points.map((point: IPoint) =>
      new b2Vec2(point.x / scale, point.y / scale)
    );
  }

  protected init(): void {
    super.init();
    this._fixDef.shape = b2PolygonShape.AsArray(this._vertices);
  }

}
