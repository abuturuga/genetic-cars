import { Body } from './body';

export class Circle extends Body {

  private radius: number = 0;

  constructor() {
    super();
  }

  setRadius(r: number): void {
    this.radius = r;
  }
  
  protected init(): void {
    super.init();
    this._fixDef.shape = new Box2D.Collision.Shapes.b2CircleShape(this.radius);
    this._fixDef.density = 1.0;
  }

}