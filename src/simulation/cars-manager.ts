import { Car, ICarConfig, ICar, ICarState } from "./car/car";

export class CarsManager {

  private _cars: ICar[] = [];

  public get cars(): ICar[] { return this._cars; }

  public getLider(cars: Car[]): Car {
    let lider = 0,
        prevPos = 0;
  
    cars.forEach((car, index) => {
      const position = car.getPosition();
  
      if(prevPos < position.x) {
        prevPos = position.x;
        lider = index;
      }
    });
  
    return cars[lider];
  }

  public filterCars(carsList: Car[], world: Box2D.Dynamics.b2World): Car[] {
    return carsList.filter((car: Car) => {
      car.update();
  
      if (!car.isAlive()) {
        car.destroy(world);
        return false;
      }
  
      return true;
    });
  }

  public buildCars(carsConf: ICarConfig[], world: Box2D.Dynamics.b2World): Car[] {
    this._cars = [];

    return carsConf.map((config: ICarConfig) => {
      const car = new Car(config);
      car.attachToWorld(world);
      this._cars.push({config, state: car.state});
      return car;
    });
  }

  public updateCarsState(cars: Car[]): void {
    this._cars.forEach((carState: ICar) => {
      const car: Car = cars.find((c) => c.getConfig().id === carState.config.id);

      if (!car) {
        carState.state.health = 0;
      } else {
        carState.state = car.state;
      }
    });
  }

}